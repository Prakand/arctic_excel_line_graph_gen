from django.db import models

class Deadline(models.Model):
    ip_family = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    date = models.DateField()
