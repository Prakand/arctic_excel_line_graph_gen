import pandas as pd
import matplotlib.pyplot as plt
from io import BytesIO
import base64
from django.shortcuts import render
from django.http import HttpResponse
from .models import Deadline
from dateutil import parser

def upload_file(request):
    if request.method == 'POST':
        file = request.FILES.get('file')

        if not file:
            return render(request, 'upload.html', {'error_message': 'No file selected.'})

        # Read the Excel file
        df = pd.read_excel(file)

        # Process the data and save it to the database
        deadlines = []
        for _, row in df.iterrows():
            deadline = Deadline(
                ip_family=row['IPFamily'],
                name=row['Deadline Name'],
                date=parser.parse(str(row['Deadline Date'])).strftime('%Y-%m-%d')
            )
            deadlines.append(deadline)

        Deadline.objects.bulk_create(deadlines)

        # Generate the line graph
        graph_data = Deadline.objects.values('ip_family').distinct()
        graphs = []
        for data in graph_data:
            ip_family = data['ip_family']
            deadlines = Deadline.objects.filter(ip_family=ip_family).order_by('date')
            dates = [deadline.date for deadline in deadlines]
            names = [deadline.name for deadline in deadlines]

            plt.plot(dates, names)
            plt.xlabel('Deadline Date')
            plt.ylabel('Deadline Name')
            plt.title(f'IP Family: {ip_family}')

            # Save the graph to a BytesIO buffer
            buffer = BytesIO()
            plt.savefig(buffer, format='png')
            buffer.seek(0)

            # Convert the buffer to a base64-encoded string
            graph = buffer.getvalue()
            graph_encoded = base64.b64encode(graph).decode('utf-8')

            graphs.append(graph_encoded)

            # Clear the plot for the next iteration
            plt.clf()

        return render(request, 'graph.html', {'graphs': graphs})

    return render(request, 'upload.html')
